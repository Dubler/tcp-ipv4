#include <string.h>
#include "tcp_ipv4.h"
#include <arpa/inet.h>
#include <stdio.h>

int openSocket(){
    int s = socket (PF_INET, SOCK_RAW, IPPROTO_TCP);

    if(s == -1)
    {
        //socket creation failed, may be because of non-root privileges
        perror("Failed to create socket");
        exit(1);
    }

    //IP_HDRINCL to tell the kernel that headers are included in the packet
    int one = 1;
    const int *val = &one;

    if (setsockopt (s, IPPROTO_IP, IP_HDRINCL, val, sizeof (one)) < 0)
    {
        printf("Error setting IP_HDRINCL");
        exit(1);
    }
    return s;
}

void selectInterface(int socket, const char *interface){
    if (setsockopt(socket, SOL_SOCKET, SO_BINDTODEVICE, interface, strlen(interface))) {
        printf("Error setting interface %s", interface);
        exit(0);
    }
}

tcp_ipv4_packet *createPacket(){
    tcp_ipv4_packet *packet = malloc(sizeof(tcp_ipv4_packet));
    packet->datagram = malloc(DATAGRAM_SIZE*sizeof(char));
    memset(packet->datagram,0,DATAGRAM_SIZE); //zero out memory

    packet->ipHeader = (struct iphdr *) packet->datagram;
    packet->tcpHeader = (struct tcphdr *) (packet->datagram + sizeof (struct ip));

    packet->dest_ip_struct = malloc(sizeof(struct sockaddr_in));
    return packet;
}

void fillData(tcp_ipv4_packet *packet, const char *data){
    packet->data = (packet->datagram + sizeof(struct iphdr) + sizeof(struct tcphdr));
    strcpy(packet->data, data);
}

void fillTransportationAddresses(tcp_ipv4_packet* packet, const char * source_ip, const char *destination_ip, unsigned short destination_port){
    strcpy(packet->destination_ip , destination_ip);
    packet->dest_ip_struct->sin_family = AF_INET;
    packet->dest_ip_struct->sin_port = htons(destination_port);
    packet->dest_ip_struct->sin_addr.s_addr = inet_addr (source_ip);
}

void fillIpHeader(tcp_ipv4_packet* packet, u_int8_t tos, u_int16_t id, u_int8_t ttl)
{
    packet->ipHeader->ihl = 5;
    packet->ipHeader->version = 4;
    packet->ipHeader->tos = tos;
    packet->ipHeader->tot_len = sizeof (struct iphdr) + sizeof (struct tcphdr) + strlen(packet->data);
    packet->ipHeader->id = htons(id); //Id of this packet, random one because we're not fragmenting packets
    packet->ipHeader->frag_off = 0; // do not fragment
    packet->ipHeader->ttl = ttl;
    packet->ipHeader->protocol = IPPROTO_TCP;
    packet->ipHeader->check = 0;      //Set to 0 before calculating checksum
    packet->ipHeader->saddr = inet_addr ( packet->destination_ip );    //Spoof the source ip address
    packet->ipHeader->daddr = packet->dest_ip_struct->sin_addr.s_addr;

    packet->ipHeader->check = calculateChecksum ((unsigned short *) packet->datagram, packet->ipHeader->tot_len);
}

/**
 * @todo fixme later
 */
void fillTcpHeader(tcp_ipv4_packet* packet, unsigned short source_port, unsigned short sequence_number,
bool ack_flag, unsigned int acknowledgement_number, bool urg_flag, unsigned short urgent_pointer,
unsigned short doff, bool fin_flag, bool syn_flag, bool rst_flag, bool psh_flag)
{
    packet->tcpHeader->source = htons (source_port);
    packet->tcpHeader->dest = packet->dest_ip_struct->sin_port;
    packet->tcpHeader->seq = htonl(sequence_number); // stack overflow says that tcp supports only 16 bit seq (which is probably bullshit)
    if (ack_flag == true) {
        packet->tcpHeader->ack_seq = htonl(acknowledgement_number);
        packet->tcpHeader->ack = 1;
    } else {
        packet->tcpHeader->ack_seq = 0;
        packet->tcpHeader->ack = 0;
    }

    //reserved 4 bits, should by always 0, according to Linux Kernel Networking: Implementation and Theory
    packet->tcpHeader->res1 = 0;

    packet->tcpHeader->doff = doff;  //data offset (tcp header size) minimum 5 - 20bytes, maximum 15 - 60bytes
    packet->tcpHeader->fin = (u_int16_t)fin_flag; // no more data from sender
    packet->tcpHeader->syn = (u_int16_t)syn_flag; // 3 way handshake
    packet->tcpHeader->rst = (u_int16_t)rst_flag; // if 1 then segment is not intended for the current connection arrives
    packet->tcpHeader->psh = (u_int16_t)psh_flag; // if 1 data should be passed to user space as soon as possible

    if (urg_flag == true) {
        packet->tcpHeader->urg = 1; // if 1 urgent pointer is meaningfull
        packet->tcpHeader->urg_ptr = htons(urgent_pointer); // urgent pointer, indicates last urgent data byte
    } else {
        packet->tcpHeader->urg = 0; // if 1 urgent pointer is meaningfull
        packet->tcpHeader->urg_ptr = htons(0); // urgent pointer, indicates last urgent data byte
    }
    packet->tcpHeader->window = htons (5840); /* maximum allowed window size */
    packet->tcpHeader->check = 0; //leave checksum 0 now, filled later in checksum calculation

    calculateTcpHeaderChecksum(packet);
}

unsigned short calculateChecksum(unsigned short *ptr,int nbytes)
{
    register long sum; //register - declare a variable in cpu cache if possible for faster access
    unsigned short oddbyte;
    register short answer;

    sum=0;
    while(nbytes>1) {
        sum+=*ptr++;
        nbytes-=2;
    }
    if(nbytes==1) {
        oddbyte=0;
        *((u_char*)&oddbyte)=*(u_char*)ptr;
        sum+=oddbyte;
    }

    sum = (sum>>16)+(sum & 0xffff);
    sum = sum + (sum>>16);
    answer=(short)~sum;

    return (unsigned short) answer;
}

unsigned short calculateTcpHeaderChecksum(tcp_ipv4_packet* packet)
{
    struct pseudo_header psh;

    psh.source_address = packet->dest_ip_struct->sin_addr.s_addr;
    psh.dest_address = inet_addr(packet->destination_ip);
    psh.placeholder = 0;
    psh.protocol = IPPROTO_TCP;
    psh.tcp_length = htons(sizeof(struct tcphdr) + strlen(packet->data));

    int psize = sizeof(struct pseudo_header) + sizeof(struct tcphdr) + strlen(packet->data);
    char *pseudogram = malloc(psize);

    memcpy(pseudogram , (char*) &psh , sizeof (struct pseudo_header));
    memcpy(pseudogram + sizeof(struct pseudo_header) , packet->tcpHeader , sizeof(struct tcphdr) + strlen(packet->data));

    packet->tcpHeader->check = calculateChecksum( (unsigned short*) pseudogram , psize);
}

int sendPacket(tcp_ipv4_packet *packet, int socket, int n)
{
    while (n > 0) {
        if (sendto(socket, packet->datagram, packet->ipHeader->tot_len, 0, (struct sockaddr *) packet->dest_ip_struct,
                   sizeof(struct sockaddr_in)) < 0) {
            perror("sendto failed");
        } else {
            printf("Packet Send. Length : %d \n", packet->ipHeader->tot_len);
        }
        n--;
    }
}

void destroyPacket(tcp_ipv4_packet *packet){
    free(packet->dest_ip_struct);
    free(packet->datagram);
    free(packet);
}

void closeSocket(int socket){
    shutdown(socket,SHUT_RDWR);
}