#include "list.h"
#include <stdlib.h>
#include <stdio.h>

list_t* create_list(void){
    list_t *newlist;
    newlist = malloc(sizeof(list_t));
    if(newlist == NULL){
        return NULL;
    }
    newlist->head = NULL;
    newlist->tail = NULL;
    return newlist;
}

void delete_list(list_t *list){
    node_t *current_node = list->head;
    node_t *next_node = NULL;
    while (current_node != NULL){
        next_node = current_node->next;
        destroyPacket(current_node->packet);
        free(current_node);
        current_node = next_node;
    }
    free(list);
}

node_t *get_node(list_t *list, unsigned int n){

    int iterator = 0;
    node_t *current_node = list->head;
    if (current_node == NULL) {
        return NULL;
    }
    while (iterator != n && current_node != NULL){
        iterator++;
        current_node = current_node->next;
    }

    return current_node;
}

unsigned int get_list_length(list_t *list){
    int length = 0;
    node_t* current_node = list->head;
    while (current_node->next != NULL){
        current_node = current_node->next;
        length++;
    }
    return length+1;
}

void push_node(list_t *list, tcp_ipv4_packet *packet){
    node_t *new_node = malloc(sizeof(node_t));
    new_node->packet = packet;
    new_node->next = NULL;

    //if list empty
    if (list->tail == NULL){
        new_node->prev = NULL;
        list->head = new_node;
        list->tail = new_node;
    } else {
        new_node->prev = list->tail;
        list->tail->next = new_node;
        list->tail = new_node;
    }
}


void remove_node(list_t *list, unsigned int index){
    //remove if index is first element of the list
    if (index == 0){
        node_t *current_node = list->head;
        list->head = current_node->next;
        current_node->next->prev = NULL;
        destroyPacket(current_node->packet);
        free(current_node);
        return;
    }
    node_t *prev_node = get_node(list, index-1);
    //remove if index is last element of the list
    if (prev_node->next == list->tail){
        list->tail = prev_node;
        destroyPacket(prev_node->next->packet);
        free(prev_node->next);
        prev_node->next = NULL;
        return;
    } else {
        //remove in any other case
        node_t *next_node = prev_node->next->next;
        destroyPacket(prev_node->next->packet);
        free(prev_node->next);
        prev_node->next = next_node;
        next_node->prev = prev_node;
    }
}
