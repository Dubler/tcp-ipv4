#ifndef C2_PROJEKT_TCP_IPV4_H
#define C2_PROJEKT_TCP_IPV4_H

#include <sys/socket.h>    //for sockets
#include <stdlib.h> //for exit(0);
#include <errno.h> //For errno
#include <netinet/tcp.h>   //for tcp header
#include <netinet/ip.h>    //for ip header
#include <netinet/in.h>
#include <stdbool.h>

#define DATAGRAM_SIZE 4096

typedef struct tcp_ipv4_packet tcp_ipv4_packet;
struct tcp_ipv4_packet {
    char *datagram, *data;

    struct iphdr *ipHeader;
    struct tcphdr *tcpHeader;

    char destination_ip[32];
    struct sockaddr_in *dest_ip_struct;
};


//public
tcp_ipv4_packet *createPacket();
int openSocket();
void closeSocket(int socket);
void selectInterface(int socket, const char *interface);
int sendPacket(tcp_ipv4_packet *packet, int socket, int n);
void fillData(tcp_ipv4_packet *packet, const char *data);
void fillTransportationAddresses(tcp_ipv4_packet* packet, const char * source_ip, const char *destination_ip, unsigned short destination_port);
void fillIpHeader(tcp_ipv4_packet* packet, u_int8_t tos, u_int16_t id, u_int8_t ttl);
void fillTcpHeader(tcp_ipv4_packet* packet, unsigned short source_port, unsigned short sequence_number,
                   bool ack_flag, unsigned int acknowledgement_number, bool urg_flag, unsigned short urgent_pointer,
                   unsigned short doff, bool fin_flag, bool syn_flag, bool rst_flag, bool psh_flag);
void destroyPacket(tcp_ipv4_packet *packet);

//private
unsigned short calculateChecksum(unsigned short *ptr,int nbytes);
unsigned short calculateTcpHeaderChecksum(tcp_ipv4_packet* packet);

/*
    96 bit (12 bytes) pseudo header needed for tcp header checksum calculation
*/
struct pseudo_header
{
    u_int32_t source_address;
    u_int32_t dest_address;
    u_int8_t placeholder;
    u_int8_t protocol;
    u_int16_t tcp_length;
};


#endif //C2_PROJEKT_TCP_IPV4_H
