#ifndef _list_H_
#define _list_H_

#include "tcp_ipv4.h"

typedef struct node_t node_t;
struct node_t{
    tcp_ipv4_packet *packet;
    node_t *next;
    node_t *prev;
};

typedef struct{
    node_t *head; // first node
    node_t *tail; // last node
} list_t;

list_t* create_list(void);
void delete_list(list_t *list);
node_t *get_node(list_t *list, unsigned int n);
unsigned int get_list_length(list_t *list);
void push_node(list_t *list, tcp_ipv4_packet *packet);
void remove_node(list_t *list, unsigned int index);

#endif
