cmake_minimum_required(VERSION 3.8)
project(c2_projekt)

set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES main.c tcp_ipv4.h tcp_ipv4.c list.h list.c)
add_executable(c2_projekt ${SOURCE_FILES})