#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include "tcp_ipv4.h"
#include "list.h"

/* Flag set by ‘--verbose’. */
static int verbose_flag;

int main (int argc, char **argv)
{
    if (argc < 2) {
        printf("usage: ./file [arguments]\n");
        printf("Available arguments:\n");
        printf("--verbose - show extra informations\n");
        printf("-a --p    - packet payload [default: none]\n");
        printf("-b --i    - interface [default: lo]\n");
        printf("-c --n    - how many copies of packet [default: 1]\n");
        printf("-d --pid  - packet id [default: 0]\n");
        printf("-e --pds  - destination port [default: 0]\n");
        printf("-f --psr  - source port [default: 0]\n");
        printf("-g --dst  - destination ip [default: 127.0.0.2]");
        printf("-h --src  - source ip [default: 127.0.0.1]");
        printf("-i --tos  - type of service [default: 0");
        printf("-j --ttl  - type of service [default: 255");
        printf("-k --dof  - data offset [default: 5");
        printf("-l --urg  - urgent pointer value [default: 0");
        printf("-m --seq  - sequence value [default: 0");
        printf("-n --seq  - ack value [default: 0");
        printf("-w --fin  - finish flag");
        printf("-x --syn  - 3 way handshake flag");
        printf("-y --rst  - reset flag");
        printf("-z --psh  - psh flag");

        return 0;
    }



    //default values
    char packet_addr_destination[32] = "127.0.0.1";
    char packet_addr_source[32] = "127.0.0.2";
    char data[2048] = "";
    char interface[32] = "lo";
    unsigned short packets_ammount = 1;
    unsigned short packet_port_dest = 0;
    unsigned short packet_port_source = 0;
    unsigned char packet_tos = 0;
    unsigned short packet_packet_id = 0;
    unsigned char packet_ttl = 255;
    unsigned short packet_sequence = 0;
    bool packet_flag_ack = false;
    unsigned int packet_ack_number = 0;
    bool packet_flag_urg = false;
    unsigned short packet_urg_ptr = 0;
    unsigned short packet_doff = 5;
    bool packet_flag_fin = false;
    bool packet_flag_syn = false;
    bool packet_flag_rst = false;
    bool packet_flag_psh = false;

    int c;

    static struct option long_options[] =
            {
                    {"verbose", no_argument,       &verbose_flag, 1},
                    {"p",       required_argument, 0, 'a'}, // payload
                    {"i",       required_argument, 0, 'b'}, // interface
                    {"n",       required_argument, 0, 'c'}, // how many packets
                    {"pid",     required_argument, 0, 'd'}, // packet id
                    {"pds",     required_argument, 0, 'e'}, // destination port
                    {"psr",     required_argument, 0, 'f'}, // source port
                    {"dst",     required_argument, 0, 'g'}, // destination ip
                    {"src",     required_argument, 0, 'h'}, // source ip
                    {"tos",     required_argument, 0, 'i'}, // ipheader tos
                    {"ttl",     required_argument, 0, 'j'}, // ttl
                    {"dof",     required_argument, 0, 'k'}, // dof
                    {"urg",     required_argument, 0, 'l'}, // urgent pointer
                    {"seq",     required_argument, 0, 'm'}, // sequence
                    {"ack",     required_argument, 0, 'n'}, // ack
                    {"fin",     no_argument,       0, 'w'}, // flag fin
                    {"syn",     no_argument,       0, 'x'}, // flag syn
                    {"rst",     no_argument,       0, 'y'}, // flag rst
                    {"psh",     no_argument,       0, 'z'}, // flag psh
            };

    while (1)
    {
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long (argc, argv, "a:b:c:d:e:f:g:h:i:j:k:l:m:n:o:wxyz",
                         long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
            break;

        switch (c)
        {
            case 'a': //data
                sprintf(data,"%s",optarg);
                break;

            case 'b': //interface
                sprintf(interface,"%s",optarg);
                break;

            case 'c': // how many packets
                packets_ammount = (unsigned short) atoi(optarg);
                break;

            case 'd': // packet id
                packet_packet_id = (unsigned short) atoi(optarg);
                break;

            case 'e': // destination port
                packet_port_dest = (unsigned short) atoi(optarg);
                break;

            case 'f': // source port
                packet_port_source = (unsigned short) atoi(optarg);
                break;

            case 'g': // destination ip
                sprintf(packet_addr_destination,"%s",optarg);
                break;

            case 'h': // source ip
                sprintf(packet_addr_source,"%s",optarg);
                break;

            case 'i': // tos
                packet_tos = (unsigned char) atoi(optarg);
                break;

            case 'j': // ttl
                packet_ttl = (unsigned char) atoi(optarg);
                break;

            case 'k': // doff
                packet_doff = (unsigned short) atoi(optarg);
                break;

            case 'l': // urgent pointer
                packet_flag_urg = true;
                packet_urg_ptr = (unsigned short) atoi(optarg);
                break;

            case 'm': // sequence
                packet_flag_urg = true;
                packet_sequence = (unsigned short) atoi(optarg);
                break;

            case 'n': // ack
                packet_flag_ack = true;
                packet_ack_number = (unsigned int) atol(optarg);
                break;

            //flags
            case 'w':
                packet_flag_fin = true;
                break;

            case 'x':
                packet_flag_syn = true;
                break;

            case 'y':
                packet_flag_rst = true;
                break;

            case 'z':
                packet_flag_psh = true;
                break;

            default:
                abort();
        }
    }

    /* Instead of reporting ‘--verbose’
       and ‘--brief’ as they are encountered,
       we report the final status resulting from them. */
    if (verbose_flag) {
        puts("verbose flag is set");
    }

    int s = openSocket();
    list_t* list = create_list();
    selectInterface(s, interface);

    for (int i = 0; i < packets_ammount; i++){
        tcp_ipv4_packet *packet = createPacket();

        fillData(packet, data);
        fillTransportationAddresses(packet, packet_addr_destination, packet_addr_source, packet_port_dest);
        fillIpHeader(packet, packet_tos, packet_packet_id, packet_ttl);
        fillTcpHeader(packet, packet_port_source, packet_sequence , packet_flag_ack, packet_ack_number,
                      packet_flag_urg, packet_urg_ptr, packet_doff, packet_flag_fin, packet_flag_syn, packet_flag_rst, packet_flag_psh);

        push_node(list,packet);
    }

    for (int i = 0; i < get_list_length(list); i++ ) {
        sendPacket(get_node(list,i)->packet, s, 1);
    }

    delete_list(list);
    closeSocket(s);

    exit (0);
}